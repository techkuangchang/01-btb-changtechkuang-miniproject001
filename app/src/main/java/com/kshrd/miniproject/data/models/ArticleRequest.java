package com.kshrd.miniproject.data.models;

import com.google.gson.annotations.SerializedName;

public class ArticleRequest {

    private int id;
    private String title;
    private String description;
    private String image;
    @SerializedName("created_date")
    private String createdDate;

    public ArticleRequest() {
    }

    public ArticleRequest(String title, String description, String image, String createdDate) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "ArticleRequest{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
