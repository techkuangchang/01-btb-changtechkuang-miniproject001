package com.kshrd.miniproject.data.models;

import com.google.gson.annotations.SerializedName;

public class ImageResponse {

    @SerializedName("data")
    private String image;

    public ImageResponse() {
    }

    public ImageResponse(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ImageResponse{" +
                "image='" + image + '\'' +
                '}';
    }

}
