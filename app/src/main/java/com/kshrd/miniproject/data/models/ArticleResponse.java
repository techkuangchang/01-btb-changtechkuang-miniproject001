package com.kshrd.miniproject.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ArticleResponse {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<ArticleModel> articleResponse;

    public ArticleResponse() {
    }

    public ArticleResponse(String code, String message, List<ArticleModel> articleResponse) {
        this.code = code;
        this.message = message;
        this.articleResponse = articleResponse;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ArticleModel> getArticleResponse() {
        return articleResponse;
    }

    public void setArticleResponse(List<ArticleModel> articleResponse) {
        this.articleResponse = articleResponse;
    }

    @Override
    public String toString() {
        return "ArticleResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", articleResponse=" + articleResponse +
                '}';
    }

}
