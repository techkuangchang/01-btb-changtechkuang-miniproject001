package com.kshrd.miniproject.data.remote;

import com.kshrd.miniproject.data.models.ArticleDetailModel;
import com.kshrd.miniproject.data.models.ArticleDetailResponse;
import com.kshrd.miniproject.data.models.ArticleRequest;
import com.kshrd.miniproject.data.models.ArticleResponse;
import com.kshrd.miniproject.data.models.ImageResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface APIService {

    @GET("/v1/api/articles")
    Call<ArticleResponse> findAll(
//            @Query("page") int page,
//            @Query("limit") int limit
    );

    @GET("/v1/api/articles/{id}")
    Call<ArticleDetailResponse> getByOne(@Path("id") int id);

    @PUT("/v1/api/articles/{id}")
    Call<ArticleDetailModel> updateByOne(@Path("id") int id, @Body ArticleDetailModel articleDetailModel);

    @POST("/v1/api/articles")
    Call<ArticleRequest> insert(@Body ArticleRequest articleRequest);

    @Multipart
    @POST("/v1/api/uploadfile/single")
    Call<ImageResponse> insertImage(@Part MultipartBody.Part image);

    @DELETE("/v1/api/articles/{id}")
    Call<ArticleResponse> delete(@Path("id") int id);

}
