package com.kshrd.miniproject.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleDetailResponse {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ArticleDetailModel articleDetailResponse;

    public ArticleDetailResponse() {
    }

    public ArticleDetailResponse(String code, String message, ArticleDetailModel articleDetailResponse) {
        this.code = code;
        this.message = message;
        this.articleDetailResponse = articleDetailResponse;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArticleDetailModel getArticleDetailResponse() {
        return articleDetailResponse;
    }

    public void setArticleDetailResponse(ArticleDetailModel articleDetailResponse) {
        this.articleDetailResponse = articleDetailResponse;
    }

    @Override
    public String toString() {
        return "ArticleDetailResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", articleDetailResponse=" + articleDetailResponse +
                '}';
    }

}
