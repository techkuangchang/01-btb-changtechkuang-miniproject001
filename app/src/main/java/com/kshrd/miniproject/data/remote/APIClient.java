package com.kshrd.miniproject.data.remote;

import com.kshrd.miniproject.data.models.ArticleModel;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static String BASE_URL = "http://110.74.194.124:15011/";
    private static String BASE_API_KEY = "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";

    private static OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

//    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor()
//            .setLevel(HttpLoggingInterceptor.Level.BASIC);

//    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
//            .connectTimeout(100, TimeUnit.SECONDS)
//            .readTimeout(100, TimeUnit.SECONDS)
//            .addInterceptor(loggingInterceptor)
//            .addInterceptor(new BasicInterceptorHelper());

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
//            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService (Class<S> serviceClass) {
        okHttpBuilder.addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder reqBuilder = request.newBuilder()
                        .addHeader("Authorization",BASE_API_KEY)
                        .header("Accept","application/json")
                        .method(request.method(), request.body());
                return chain.proceed(reqBuilder.build());
            }
        });

        return builder.client(okHttpBuilder.build()).build().create(serviceClass);

    }

    public static int getDataId(){
        ArticleModel articleModel = new ArticleModel();
        return articleModel.getId();
    }

}
