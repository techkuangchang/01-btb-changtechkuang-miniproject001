package com.kshrd.miniproject.view.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.kshrd.miniproject.R;
import com.kshrd.miniproject.data.models.ArticleDetailModel;
import com.kshrd.miniproject.data.models.ImageResponse;
import com.kshrd.miniproject.viewmodels.MainActivityViewModel;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddActivity extends AppCompatActivity {

    ImageView mEditArticle;
    Button btnCancel, btnSave;
    EditText mEdiTxtAdd, mEditDescription;
    TextView mDateNow;
    ImageView mImageUrl;
    ProgressBar progressBar;

    String imageName;
    final int IMAGE_CODE = 222;
    final int PERMISSION_CODE = 99;

    MainActivityViewModel AddMainActivityViewModel;
    ArticleDetailModel articleResponse = new ArticleDetailModel();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_add_custom);

        mEdiTxtAdd = findViewById(R.id.title_add);
        mDateNow = findViewById(R.id.date_add);
        mEditDescription = findViewById(R.id.description_add);
        mImageUrl = findViewById(R.id.image_url_add);
        btnSave = findViewById(R.id.btn_save);
        btnCancel = findViewById(R.id.btn_cancel);
        progressBar = findViewById(R.id.progress_loading);

        AddMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        //------------------------ Update Article By Id
        Intent intent = getIntent();
        //-------------Get Intent data from MyListAdapter
        String imageUrl = intent.getStringExtra("imageDetail");
        Glide.with(this).load(imageUrl).into(mImageUrl);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMHHmmss");
        String localeDate = intent.getStringExtra("dateDetail");
        Date date = null;
        try {
            date = dateFormat.parse(localeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        mDateNow.setText("Created Date : "+dateFormat.format(date));
        mEdiTxtAdd.setText(intent.getStringExtra("titleDetail"));
        mEditDescription.setText(intent.getStringExtra("descriptionDetail"));

        requestStoragePermission();
        mImageUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = intent.getIntExtra("idUpdate",0);
                Log.d("get","data "+id);
                articleResponse.setId(id);
                articleResponse.setTitle(mEdiTxtAdd.getText().toString());
                articleResponse.setDate(mDateNow.getText().toString());
                articleResponse.setDescription(mEditDescription.getText().toString());

                if(imageName == null){
                    progressBar.setVisibility(View.VISIBLE);
                    articleResponse.setImageUrl(imageName);
                }
                else if(imageName != null) {
                    progressBar.setVisibility(View.GONE);
                    articleResponse.setImageUrl(imageName);
                }

                AddMainActivityViewModel.updateResponseMutableLiveData(id,articleResponse).observe(AddActivity.this, new Observer<ArticleDetailModel>() {
                    @Override
                    public void onChanged(ArticleDetailModel articleResponse) {

                    }
                });
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE);
    }

    public void selectImage() {
        //intent to pick image
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent, "Select Picture"), IMAGE_CODE);
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = this.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri path = data.getData();
            Glide.with(this)
                    .load(path)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(mImageUrl);
            String imageUrl = getPath(path);
            Uri uri = Uri.parse(imageUrl);
            File file = new File(uri.getPath());
            Log.d("dataImage",imageUrl);
            Log.d("dataImage",file.toString());
            AddMainActivityViewModel.getImageDataArticle(file).observe(this, new Observer<ImageResponse>() {
                @Override
                public void onChanged(ImageResponse imageResponse) {
                    imageName = imageResponse.getImage();
                    Log.d("dataImage","Image "+ imageName);
                    System.out.println("getImage");
                }
            });
        } else {
            Toast.makeText(this, "Bitmap is NULL", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

}
