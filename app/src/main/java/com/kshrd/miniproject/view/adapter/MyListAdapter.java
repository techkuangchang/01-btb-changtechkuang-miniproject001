package com.kshrd.miniproject.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kshrd.miniproject.R;
import com.kshrd.miniproject.data.models.ArticleModel;
import com.kshrd.miniproject.data.models.ArticleResponse;
import com.kshrd.miniproject.view.main.AddActivity;
import com.kshrd.miniproject.view.main.ViewDetailActivity;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {

    public static String TAG = "MyListAdapter";

    public Context context;
    public ArticleResponse articleList;
    ArticleModel articleModel;

    public MyListAdapter(Context context, ArticleResponse articleList) {
        this.context = context;
        this.articleList = articleList;
    }

    public void setList(ArticleResponse articleList ){
        this.articleList = articleList;
        notifyDataSetChanged();
    }

    public int getArticleId(int position) {
        notifyItemRemoved(position);
        Log.d("Adapter", String.valueOf(articleList.getArticleResponse().get(position).getId()));
        notifyDataSetChanged();
        return articleList.getArticleResponse().get(position).getId();
    }

    public int getDataId(){
        return articleModel.getId();
    }

    @NonNull
    @Override
    public MyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_list_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyListAdapter.ViewHolder holder, int position) {
        holder.mTitle.setText(this.articleList.getArticleResponse().get(position).getTitle());
        Glide.with(context).load(articleList.getArticleResponse().get(position).getImageUrl())
                .into(holder.mImageView);

        //When click image and go to new activity ViewDetailActivity
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = articleList.getArticleResponse().get(position).getId();
                String titleDetail = articleList.getArticleResponse().get(position).getTitle();
                String desDetail = articleList.getArticleResponse().get(position).getDescription();
                String imageDetail = articleList.getArticleResponse().get(position).getImageUrl();
                String dateDetail = articleList.getArticleResponse().get(position).getDate();
                Intent intent = new Intent(context, ViewDetailActivity.class);
                intent.putExtra("id",id);

                intent.putExtra("imageDetail",imageDetail);
                intent.putExtra("titleDetail",titleDetail);
                intent.putExtra("dateDetail",dateDetail);
                intent.putExtra("descriptionDetail",desDetail);

                context.startActivity(intent);
            }
        });

        holder.mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Update OnClick");
                int id = articleList.getArticleResponse().get(position).getId();
                String titleDetail = articleList.getArticleResponse().get(position).getTitle();
                String desDetail = articleList.getArticleResponse().get(position).getDescription();
                String imageDetail = articleList.getArticleResponse().get(position).getImageUrl();
                String dateDetail = articleList.getArticleResponse().get(position).getDate();

                Intent intent = new Intent(context, AddActivity.class);
                intent.putExtra("idUpdate",id);
                intent.putExtra("imageDetail",imageDetail);
                intent.putExtra("titleDetail",titleDetail);
                intent.putExtra("dateDetail",dateDetail);
                intent.putExtra("descriptionDetail",desDetail);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(this.articleList != null) {
            Log.d(TAG, "Article " + articleList.getArticleResponse().size());
            return this.articleList.getArticleResponse().size();
        }
            Log.d(TAG, "Article return 0");
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public ImageView mImageView;
        public ImageView mUpdate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.txt_title);
            mImageView = itemView.findViewById(R.id.image_url);
            mUpdate = itemView.findViewById(R.id.image_edit);
        }
    }


}
