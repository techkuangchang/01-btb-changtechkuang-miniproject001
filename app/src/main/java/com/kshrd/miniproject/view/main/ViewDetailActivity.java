package com.kshrd.miniproject.view.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.kshrd.miniproject.R;
import com.kshrd.miniproject.viewmodels.MainActivityViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ViewDetailActivity extends AppCompatActivity {

    ImageView mImageDetail;
    TextView mTxtTitleDetail, mTxtDescription, mTxtDate;
    MainActivityViewModel activityViewModelDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        mImageDetail = findViewById(R.id.image_detail);
        mTxtTitleDetail = findViewById(R.id.title_detail);
        mTxtDescription = findViewById(R.id.description_detail);
        mTxtDate = findViewById(R.id.date_detail);

        activityViewModelDetail = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        //---------------- Get Image By One
        //------------------------ Get Article By Id
        Intent intent = getIntent();
        int id = intent.getIntExtra("id",0);
        activityViewModelDetail.getByOne(id);
        Log.d("get","data "+id);

        //-------------Get Intent data from MyListAdapter
        String imageUrl = intent.getStringExtra("imageDetail");
        Glide.with(this).load(imageUrl).into(mImageDetail);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMHHmmss");
        String localeDate = intent.getStringExtra("dateDetail");
        Date date = null;
        try {
            date = dateFormat.parse(localeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        mTxtDate.setText("Created Date : "+dateFormat.format(date));

        mTxtTitleDetail.setText(intent.getStringExtra("titleDetail"));
        mTxtDescription.setText(intent.getStringExtra("descriptionDetail"));
    }
}