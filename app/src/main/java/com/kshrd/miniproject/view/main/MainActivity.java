package com.kshrd.miniproject.view.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.kshrd.miniproject.R;
import com.kshrd.miniproject.data.models.ArticleRequest;
import com.kshrd.miniproject.data.models.ArticleResponse;
import com.kshrd.miniproject.data.models.ImageResponse;
import com.kshrd.miniproject.view.adapter.MyListAdapter;
import com.kshrd.miniproject.viewmodels.MainActivityViewModel;

import java.io.File;
import java.time.LocalDate;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "MainActivity";

    private MainActivityViewModel mMainActivityViewModel;
    private ArticleResponse articleModelList;

    private RecyclerView mRecyclerView;
    private MyListAdapter mAdapter;
    private FloatingActionButton mBtnFab;
    private ProgressBar mProgressBar;
    private Toolbar toolbar;
    EditText mEdiTxtAdd, mEditDescription;
    TextView mDateNow;
    ImageView mImageUrl;
    Button btnCancel, btnSave;
    int bmpImage = R.drawable.flower;
    SwipeRefreshLayout swipeRefresh;
    ArticleRequest articleRequest = new ArticleRequest();
    final int IMAGE_CODE = 222;
    final int PERMISSION_CODE = 99;

    String imageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnFab = findViewById(R.id.btn_fab);
        mProgressBar = findViewById(R.id.progress_bar);
        mRecyclerView = findViewById(R.id.recycler_view);
        swipeRefresh = findViewById(R.id.swipe_refresh);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyListAdapter(this, articleModelList);

        toolbar = findViewById(R.id.tool_bar);
        toolbar.setTitle("MiniProject");

        mMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        //------------------------ Get All

        mMainActivityViewModel.getArticleResponseLiveData().observe(MainActivity.this, new Observer<ArticleResponse>() {
            @Override
            public void onChanged(ArticleResponse articleModels) {
                swipeRefresh.setRefreshing(false);
                if(articleModels != null) {
                    articleModelList = articleModels;
                    mAdapter.setList(articleModels);
                    System.out.println("Data MEAN");
                }
                else {
                    System.out.println("Data Ot MEAN");
                }
            }
        });

        //------------------------ Refresh Layout

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mMainActivityViewModel != null){
                    hideProgressBar();
                    mMainActivityViewModel.requestArticle();
                }
                else {
                    showProgressBar();
                }
            }
        });

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        //------------------------ Add Button

        mBtnFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup viewGroup = findViewById(R.id.container);
                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.my_add_custom, viewGroup, false);

                mEdiTxtAdd = view.findViewById(R.id.title_add);
                mDateNow = view.findViewById(R.id.date_add);
                mEditDescription = view.findViewById(R.id.description_add);
                mImageUrl = view.findViewById(R.id.image_url_add);
                btnCancel = view.findViewById(R.id.btn_cancel);
                btnSave = view.findViewById(R.id.btn_save);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(view);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    LocalDate localDate = LocalDate.now();
                    mDateNow.setText(localDate.toString());
                }

                //------------------------ Image Button

                //checking the permission
                requestStoragePermission();

                mImageUrl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectImage();
                    }
                });

                //------------------------ Save Button
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            LocalDate localDate = LocalDate.now();
                            mDateNow.setText(localDate.toString());
                            articleRequest.setImage(imageName);
                            articleRequest.setTitle(mEdiTxtAdd.getText().toString().trim());
                            articleRequest.setDescription(mEditDescription.getText().toString().trim());
                            articleRequest.setCreatedDate(localDate.toString());
                            mMainActivityViewModel.postArticle(articleRequest);
                            if(mMainActivityViewModel != null) {
                                mMainActivityViewModel.postArticleRequestMutableLiveData().observe(MainActivity.this, new Observer<ArticleRequest>() {
                                    @Override
                                    public void onChanged(ArticleRequest articleRequest) {
                                        mAdapter.notifyDataSetChanged();
                                        Log.d("------>ArticleRequest","Data"+articleRequest);
                                    }
                                });
                            }
                            else {
                                Snackbar.make(v,"Please input data", Snackbar.LENGTH_SHORT).show();
                            }
                            Log.d("------>ArticleRequest","Data"+articleRequest);
                        }

                        alertDialog.dismiss();
                    }
                });

                //------------------------ Cancel Button
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

            }
        });

        //------------------------ Delete By Swipe To Right And Left
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                Toast.makeText(MainActivity.this, "Can not delete!", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int id = mAdapter.getArticleId(viewHolder.getAdapterPosition());
                mMainActivityViewModel.deleteByOne(id);
                Toast.makeText(MainActivity.this, "deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(mRecyclerView);

    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE);
    }

     public void selectImage() {
        //intent to pick image
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent, "Select Picture"), IMAGE_CODE);
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = this.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri path = data.getData();
            Glide.with(this)
                    .load(path)
                    .error(R.drawable.placeholder)
                    .into(mImageUrl);
            String imageUrl = getPath(path);
            Uri uri = Uri.parse(imageUrl);
            File file = new File(uri.getPath());
            Log.d("dataImage",imageUrl);
            Log.d("dataImage",file.toString());
            mMainActivityViewModel.getImageDataArticle(file).observe(MainActivity.this, new Observer<ImageResponse>() {
                @Override
                public void onChanged(ImageResponse imageResponse) {
                    imageName = imageResponse.getImage();
                    Log.d("dataImage","Image"+imageName);
                    System.out.println("getImage");
                }
            });
        } else {
            Toast.makeText(this, "Bitmap is NULL", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    public void scroll_to_top(MenuItem item) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        linearLayoutManager.scrollToPositionWithOffset(0, 0);
    }
}