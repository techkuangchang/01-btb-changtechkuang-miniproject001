package com.kshrd.miniproject.viewmodels;

import android.graphics.Bitmap;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kshrd.miniproject.data.models.ArticleDetailModel;
import com.kshrd.miniproject.data.models.ArticleDetailResponse;
import com.kshrd.miniproject.data.models.ArticleRequest;
import com.kshrd.miniproject.data.models.ArticleResponse;
import com.kshrd.miniproject.data.models.ImageResponse;
import com.kshrd.miniproject.data.remote.APIClient;
import com.kshrd.miniproject.data.remote.APIService;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityViewModel extends ViewModel {

    //variable pagination
    public static int PAGE = 1;
    public static int LIMIT = 5;

    Bitmap bmpImage;

    public static String TAG = "MainActivityViewModel";

    private final MutableLiveData<ArticleResponse> articleResponseLiveData;
    private final MutableLiveData<ArticleRequest> liveDataArticle;
    private MutableLiveData<ImageResponse> liveData;
    private MutableLiveData<ArticleDetailResponse> articleResponseLiveDataId = new MutableLiveData<>();
    public MutableLiveData<ArticleDetailModel> responseMutableLiveData;
    private final MutableLiveData<Boolean> mIsUpading = new MutableLiveData<>();

    public MainActivityViewModel() {
        articleResponseLiveData = new MutableLiveData<>();
        liveDataArticle = new MutableLiveData<>();
        liveData = new MutableLiveData<>();
    }

    public MutableLiveData<ArticleResponse> getArticleResponseLiveData () {
        return articleResponseLiveData;
    }

    public MutableLiveData<ArticleRequest> postArticleRequestMutableLiveData() {
        return liveDataArticle;
    }

    public MutableLiveData<ArticleDetailModel> updateResponseMutableLiveData(int id, ArticleDetailModel articleDetailModel) {
        Log.d(TAG,"Article Id "+id);
        Log.d(TAG,"Article response "+articleDetailModel);
        responseMutableLiveData = new MutableLiveData<>();
        APIClient.createService(APIService.class)
                .updateByOne(id, articleDetailModel)
                .enqueue(new Callback<ArticleDetailModel>() {
                    @Override
                    public void onResponse(Call<ArticleDetailModel> call, Response<ArticleDetailModel> response) {
                        responseMutableLiveData.postValue(response.body());
                        Log.d(TAG, "updateArticle"+response.body());
                    }

                    @Override
                    public void onFailure(Call<ArticleDetailModel> call, Throwable t) {
                        Log.e("Error Upadte", t.getMessage());
                    }
                });
        return responseMutableLiveData;
    }

    public MutableLiveData<ImageResponse> getImageDataArticle(File file) {
        liveData = new MutableLiveData<>();
        MultipartBody.Part imageUrl = MultipartBody.Part.createFormData("file", file.getName(),
                RequestBody.create(MediaType.parse("image/*"), file));
        Log.d(TAG,"----->fileName "+file.getName());
        Log.d(TAG,"----->file "+file);
        Log.d(TAG,"----->fileUrl "+imageUrl.toString());
        APIClient.createService(APIService.class)
                .insertImage(imageUrl)
                .enqueue(new Callback<ImageResponse>() {
                    @Override
                    public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                        if(response.isSuccessful() && response.body() != null){
                            liveData.postValue(response.body());
                            Log.d(TAG, "uploadImage: "+response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageResponse> call, Throwable t) {
                        Log.e("Error Image", t.getMessage());
                    }
                });

        return liveData;
    }

    public void requestArticle() {
        APIClient.createService(APIService.class)
                .findAll()
                .enqueue(new Callback<ArticleResponse>() {
                    @Override
                    public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                        articleResponseLiveData.postValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<ArticleResponse> call, Throwable t) {
                        Log.e(TAG, "Error" + t.getMessage());
                        articleResponseLiveData.postValue(null);
                    }
                });
    }

    public void postArticle(ArticleRequest articleRequest){
        mIsUpading.setValue(true);
        APIClient.createService(APIService.class)
                .insert(articleRequest)
                .enqueue(new Callback<ArticleRequest>() {
                    @Override
                    public void onResponse(Call<ArticleRequest> call, Response<ArticleRequest> response) {
                        ArticleRequest articleRequests = liveDataArticle.getValue();
                        liveDataArticle.postValue(articleRequests);
                        mIsUpading.postValue(false);
                    }

                    @Override
                    public void onFailure(Call<ArticleRequest> call, Throwable t) {
                        Log.e(TAG, "Error" + t.getMessage());
                    }
                });
    }

    public void deleteByOne(int id) {
        APIClient.createService(APIService.class)
                .delete(id)
                .enqueue(new Callback<ArticleResponse>() {
                    @Override
                    public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                        articleResponseLiveData.postValue(response.body());
                        Log.d("deleteByOne","data"+response.body());
                    }

                    @Override
                    public void onFailure(Call<ArticleResponse> call, Throwable t) {
                        Log.e("Error Delete", t.getMessage());
                    }
                });
    }

    public void getByOne(int id) {
        APIClient.createService(APIService.class)
                .getByOne(id)
                .enqueue(new Callback<ArticleDetailResponse>() {
                    @Override
                    public void onResponse(Call<ArticleDetailResponse> call, Response<ArticleDetailResponse> response) {
                        if(response.isSuccessful()) {
                        ArticleDetailResponse articleDetailResponse = articleResponseLiveDataId.getValue();
                        articleResponseLiveDataId.postValue(articleDetailResponse);
                        }
                        Log.d("getByOne","data "+response.body());
                    }

                    @Override
                    public void onFailure(Call<ArticleDetailResponse> call, Throwable t) {
                        Log.e("Error Get By One", t.getMessage());
                    }
                });
    }

}
